# CoreBB

A project showing the Architecture of ASP.NET Core Microservices Applications.

Includes:
User management - register, login, logout, access control

Forum management - create, delete, lock, update, list forums

Topic management - create, reply, delete, lock, update, list topics, list topics by category

Message management - send, read, reply, delete

Category management -  list categories