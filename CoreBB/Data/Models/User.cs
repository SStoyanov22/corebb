﻿namespace CoreBB.Web.Data.Models
{
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Collections.Generic;
    public partial class User : IdentityUser<int>
    {
        public User()
        {
            Forum = new HashSet<Forum>();
            MessageFromUser = new HashSet<Message>();
            MessageToUser = new HashSet<Message>();
            TopicCategory = new HashSet<Topic>();
            TopicModifiedByUser = new HashSet<Topic>();
            TopicOwner = new HashSet<Topic>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAdministrator { get; set; }
        public bool IsLocked { get; set; }
        public DateTime RegisterDateTime { get; set; }
        public DateTime LastLogInDateTime { get; set; }

        public virtual ICollection<Forum> Forum { get; set; }
        public virtual ICollection<Message> MessageFromUser { get; set; }
        public virtual ICollection<Message> MessageToUser { get; set; }
        public virtual ICollection<Topic> TopicCategory { get; set; }
        public virtual ICollection<Topic> TopicModifiedByUser { get; set; }
        public virtual ICollection<Topic> TopicOwner { get; set; }
    }
}
