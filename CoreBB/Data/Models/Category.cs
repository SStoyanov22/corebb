﻿using System.Collections.Generic;

namespace CoreBB.Web.Data.Models
{
    public partial class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<Topic> TopicCategory { get; set; }
    }
}
