#pragma checksum "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b8fc97681813a1b8c40f00619b3dee5cf4fab4e7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Topic_Detail), @"mvc.1.0.view", @"/Views/Topic/Detail.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b8fc97681813a1b8c40f00619b3dee5cf4fab4e7", @"/Views/Topic/Detail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a9af4978b9c2bfca24ef48e96efe5f8573634464", @"/Views/_ViewImports.cshtml")]
    public class Views_Topic_Detail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CoreBB.Web.Data.Models.Topic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml"
  
    ViewBag.Title = Model.Title;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h3>Original</h3>\r\n");
#nullable restore
#line 7 "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml"
Write(Html.Partial("_TopicReading", Model));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<hr />\r\n<h3>Replies</h3>\r\n");
#nullable restore
#line 10 "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml"
 foreach (var t in Model.InverseReplyToTopic)
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml"
Write(Html.Partial("_TopicReading", t));

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\gitlab\corebb\CoreBB\Views\Topic\Detail.cshtml"
                                     ;
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CoreBB.Web.Data.Models.Topic> Html { get; private set; }
    }
}
#pragma warning restore 1591
